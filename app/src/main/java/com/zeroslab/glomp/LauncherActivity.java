package com.zeroslab.glomp;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class LauncherActivity extends AppCompatActivity {

    Button login_button, nextImage, nextImage2, nextImageFinish;
    TextView signUp, _1stLine, _2ndLine;
    ImageView welcomeImage, logo;
    LinearLayout welcomeLayout;
    FrameLayout frameLayout;
    ProgressBar regProgressBar;

    public static int APP_REQUEST_CODE = 99;
    private int nextPermissionsRequestCode = 4000;
    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();
    private interface OnCompleteListener {
        void onComplete();
    }

    private static final String url="http://zeroslab.com/app/glomp/reg.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AccountKit.initialize(this);
        setContentView(R.layout.activity_launcher);

        if (AccountKit.getCurrentAccessToken() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            welcomeLayout=(LinearLayout)findViewById(R.id.welcomeLayout);
            welcomeLayout.setVisibility(View.VISIBLE);
            frameLayout=(FrameLayout)findViewById(R.id.frameLayout);
            logo=(ImageView)findViewById(R.id.logo);
            welcomeImage=(ImageView)findViewById(R.id.welcomeImage);
            _1stLine=(TextView)findViewById(R.id._1stLine);
            _2ndLine=(TextView)findViewById(R.id._2ndLine);
            nextImage=(Button) findViewById(R.id.next);
            nextImage2=(Button) findViewById(R.id.next2);
            nextImageFinish=(Button) findViewById(R.id.nextImageFinish);
            login_button=(Button)findViewById(R.id.login_button);
            regProgressBar=(ProgressBar)findViewById(R.id.regProgressBar);
            String htmlString="<u>SIGNUP</u>";

            signUp=(TextView)findViewById(R.id.signup_button);
            signUp.setText(Html.fromHtml(htmlString));

            Typeface _1stTypeFace = Typeface.createFromAsset(getAssets(), "fonts/ColorTime.ttf");
            Typeface _2ndTypeFace = Typeface.createFromAsset(getAssets(), "fonts/bluefires_sample.ttf");
            _1stLine.setTypeface(_1stTypeFace);
            _2ndLine.setTypeface(_2ndTypeFace);

            Picasso.with(getApplicationContext()).load(R.drawable.slider1).into(welcomeImage);
            _1stLine.setText("FINDER");
            _2ndLine.setText("Nearby Place");
            Animation animationLeft= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lefttoright);
            Animation animationRight= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
            Animation animationFadeIn= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
            _1stLine.setAnimation(animationLeft);
            _2ndLine.setAnimation(animationRight);
            welcomeImage.setAnimation(animationFadeIn);

            nextImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    nextImage.setVisibility(View.GONE);
                    nextImage2.setVisibility(View.VISIBLE);
                    Picasso.with(getApplicationContext()).load(R.drawable.slider2).into(welcomeImage);
                    _1stLine.setText("MEGA DISCOUNT");
                    _2ndLine.setText("Reserved Our App");
                    Animation animationLeft= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lefttoright);
                    Animation animationRight= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
                    Animation animationFadeIn= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                    _1stLine.setAnimation(animationLeft);
                    _2ndLine.setAnimation(animationRight);
                    welcomeImage.setAnimation(animationFadeIn);
                }
            });

            nextImage2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    nextImage2.setVisibility(View.GONE);
                    nextImageFinish.setVisibility(View.VISIBLE);
                    Picasso.with(getApplicationContext()).load(R.drawable.slider3).into(welcomeImage);
                    _1stLine.setText("FOR HUNGRY PEOPLE");
                    _2ndLine.setText("Search Food & Order");
                    Animation animationLeft= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lefttoright);
                    Animation animationRight= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
                    Animation animationFadeIn= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                    _1stLine.setAnimation(animationLeft);
                    _2ndLine.setAnimation(animationRight);
                    welcomeImage.setAnimation(animationFadeIn);
                }
            });

            nextImageFinish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    welcomeLayout.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.VISIBLE);
                    Animation animationFadeIn= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                    Animation animationUp= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.downtoup);
                    logo.setAnimation(animationFadeIn);
                    login_button.setAnimation(animationUp);
                    signUp.setAnimation(animationUp);
                }
            });


            login_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLogin();
                }
            });

            signUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLogin();
                }
            });
        }
    }

    private void onLogin() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE,
                AccountKitActivity.ResponseType.TOKEN);
        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configuration);
        OnCompleteListener completeListener = new OnCompleteListener() {
            @Override
            public void onComplete() {
                startActivityForResult(intent, APP_REQUEST_CODE);
            }
        };

        if (configuration.isReceiveSMSEnabled() && !canReadSmsWithoutPermission()) {
            final OnCompleteListener receiveSMSCompleteListener = completeListener;
            completeListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(android.Manifest.permission.ACCESS_FINE_LOCATION, R.string.permissions_receive_sms_title, R.string.permissions_receive_sms_message, receiveSMSCompleteListener);
                }
            };
        }
        if (configuration.isReadPhoneStateEnabled() && !isGooglePlayServicesAvailable()) {
            final OnCompleteListener readPhoneStateCompleteListener = completeListener;
            completeListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(android.Manifest.permission.READ_PHONE_STATE, R.string.permissions_read_phone_state_title, R.string.permissions_read_phone_state_message, readPhoneStateCompleteListener);
                }
            };
        }
        completeListener.onComplete();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                //showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Login Success";
                } else {
                    toastMessage = "Login Success";
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
                getCallBack();
            }

            // Surface the result to your user in an appropriate way.
            //Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show();
        }
    }

    public void getCallBack(){
        frameLayout.setVisibility(View.GONE);
        regProgressBar.setVisibility(View.VISIBLE);
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String id = account.getId();

                final PhoneNumber number = account.getPhoneNumber();
                String phoneNumber = number == null ? null : number.toString();

                //setRegUser(phoneNumber);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }

            @Override
            public void onError(final AccountKitError error) {
                //TODO: error handling
            }
        });
    }


    private void setRegUser(final String phoneNumber){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        goToMyLoggedInActivity();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override

            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();

                map.put("phone", phoneNumber);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void goToMyLoggedInActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show();
        LauncherActivity.this.finish();
    }

    private boolean isGooglePlayServicesAvailable() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        return googlePlayServicesAvailable == ConnectionResult.SUCCESS;
    }

    private boolean canReadSmsWithoutPermission() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
            return true;
        }
        //TODO we should also check for Android O here t18761104

        return false;
    }

    private void requestPermissions(final String permission, final int rationaleTitleResourceId, final int rationaleMessageResourceId, final OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(permission, rationaleTitleResourceId, rationaleMessageResourceId, listener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(final String permission, final int rationaleTitleResourceId, final int rationaleMessageResourceId, final OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            requestPermissions(new String[] { permission }, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            // ignore and clean up the listener
                            permissionsListeners.remove(requestCode);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{ permission }, requestCode);
        }
    }

    @TargetApi(23)
    @SuppressWarnings("unused")
    @Override
    public void onRequestPermissionsResult(final int requestCode, final @NonNull String permissions[], final @NonNull int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}