package com.zeroslab.glomp.Model;

/**
 * Created by Farjanul Nayem on 25-Aug-17.
 */

public class Weather {

    public String high;
    public String low;
    public String status;
    public String temp;
    public String city;
    public String country;

    public Weather(String high, String low, String status, String temp, String city, String country) {
        this.high = high;
        this.low = low;
        this.status = status;
        this.temp = temp;
        this.city = city;
        this.country = country;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
