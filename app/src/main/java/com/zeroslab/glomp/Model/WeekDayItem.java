package com.zeroslab.glomp.Model;

/**
 * Created by Farjanul Nayem on 12-Sep-17.
 */

public class WeekDayItem {

    private String day;

    public WeekDayItem() {
    }

    public WeekDayItem(String day) {
        this.day = day;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
