package com.zeroslab.glomp.Model;

/**
 * Created by Farjanul Nayem on 11-Sep-17.
 */

public class HomeModel {
    private String title;
    private int icon;

    public HomeModel(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
