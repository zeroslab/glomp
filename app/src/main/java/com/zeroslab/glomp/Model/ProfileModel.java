package com.zeroslab.glomp.Model;

/**
 * Created by Farjanul Nayem on 20-Sep-17.
 */

public class ProfileModel {

    private String name;
    private String phone;

    public ProfileModel(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
