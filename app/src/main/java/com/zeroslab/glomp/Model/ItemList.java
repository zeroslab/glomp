package com.zeroslab.glomp.Model;

/**
 * Created by Farjanul Nayem on 25-Aug-17.
 */

public class ItemList {

    private String nameItemList;
    private String vicinityItemList;
    private String latitudeItemList;
    private String longitudeItemList;
    private String placeIdItemList;

    public ItemList() {
    }

    public ItemList(String nameItemList, String vicinityItemList, String latitudeItemList, String longitudeItemList, String placeIdItemList) {
        this.nameItemList = nameItemList;
        this.vicinityItemList = vicinityItemList;
        this.latitudeItemList = latitudeItemList;
        this.longitudeItemList = longitudeItemList;
        this.placeIdItemList = placeIdItemList;
    }

    public String getNameItemList() {
        return nameItemList;
    }

    public void setNameItemList(String nameItemList) {
        this.nameItemList = nameItemList;
    }

    public String getVicinityItemList() {
        return vicinityItemList;
    }

    public void setVicinityItemList(String vicinityItemList) {
        this.vicinityItemList = vicinityItemList;
    }

    public String getLatitudeItemList() {
        return latitudeItemList;
    }

    public void setLatitudeItemList(String latitudeItemList) {
        this.latitudeItemList = latitudeItemList;
    }

    public String getLongitudeItemList() {
        return longitudeItemList;
    }

    public void setLongitudeItemList(String longitudeItemList) {
        this.longitudeItemList = longitudeItemList;
    }

    public String getPlaceIdItemList() {
        return placeIdItemList;
    }

    public void setPlaceIdItemList(String placeIdItemList) {
        this.placeIdItemList = placeIdItemList;
    }
}
