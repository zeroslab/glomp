package com.zeroslab.glomp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.squareup.picasso.Picasso;
import com.zeroslab.glomp.LauncherActivity;
import com.zeroslab.glomp.MainActivity;
import com.zeroslab.glomp.ProfileUpdate.AgeUpdate;
import com.zeroslab.glomp.ProfileUpdate.EmailUpdate;
import com.zeroslab.glomp.ProfileUpdate.GenderUpdate;
import com.zeroslab.glomp.ProfileUpdate.LivingLocationUpdate;
import com.zeroslab.glomp.ProfileUpdate.NameUpdate;
import com.zeroslab.glomp.ProfileUpdate.ProfessionUpdate;
import com.zeroslab.glomp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentProfile extends AppCompatActivity {

    String userName, userPhoneNo;
    String baseUrl="http://zeroslab.com/app/glomp/profile.php?phone=";
    TextView phoneNo, name, email, gender, age, profession, livingLocation, toolbarName;
    ImageView etBtPhone, etBtName, etBtEmail, etBtGender, etBtAge, etBtLivingLocation, etBtProfession;
    ProgressBar profileProgressBar;
    LinearLayout linearLayout;
    CircleImageView circleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountKit.initialize(getApplicationContext());
        setContentView(R.layout.activity_fragment_profile);
        getSupportActionBar().hide();

        linearLayout=(LinearLayout)findViewById(R.id.allInfo);
        profileProgressBar=(ProgressBar)findViewById(R.id.profileProgressBar);
        name=(TextView) findViewById(R.id.userName);
        phoneNo=(TextView) findViewById(R.id.phoneNo);
        email=(TextView) findViewById(R.id.email);
        gender=(TextView) findViewById(R.id.gender);
        age=(TextView) findViewById(R.id.age);
        profession=(TextView) findViewById(R.id.profession);
        livingLocation=(TextView) findViewById(R.id.living);
        toolbarName=(TextView) findViewById(R.id.toolbarName);
        circleImageView=(CircleImageView)findViewById(R.id.profile_image);

        //etBtName=(ImageView)findViewById(R.id.etBtAge);
        etBtGender=(ImageView)findViewById(R.id.etBtGender);
        etBtAge=(ImageView)findViewById(R.id.etBtAge);
        etBtLivingLocation=(ImageView)findViewById(R.id.etBtLivingLocation);
        etBtProfession=(ImageView)findViewById(R.id.etBtProfession);
        etBtEmail=(ImageView)findViewById(R.id.etBtEmail);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), NameUpdate.class));
            }
        });

        etBtGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), GenderUpdate.class));
            }
        });

        etBtAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AgeUpdate.class));
            }
        });

        etBtLivingLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LivingLocationUpdate.class));
            }
        });

        etBtProfession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfessionUpdate.class));
            }
        });

        etBtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EmailUpdate.class));
            }
        });

        getCallBack();
    }

    public void getCallBack(){
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String id = account.getId();

                final PhoneNumber number = account.getPhoneNumber();
                String phoneNumber = number == null ? null : number.toString();

                String url=baseUrl+phoneNumber;
                getProfileData(url);
            }

            @Override
            public void onError(final AccountKitError error) {
                //TODO: error handling
            }
        });
    }

    private void getProfileData(final String url) {
        profileProgressBar.setVisibility(View.VISIBLE);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new com.android.volley.Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jsonObject = response.getJSONObject(i);

                        name.setText(jsonObject.getString("name"));
                        toolbarName.setText(jsonObject.getString("name"));
                        phoneNo.setText(jsonObject.getString("phone"));
                        email.setText(jsonObject.getString("email"));
                        gender.setText(jsonObject.getString("gender"));
                        age.setText(jsonObject.getString("age"));
                        profession.setText(jsonObject.getString("profession"));
                        livingLocation.setText(jsonObject.getString("living"));
                        Picasso.with(getApplicationContext()).load(R.drawable.me).resize(150, 150).into(circleImageView);
                    }

                    profileProgressBar.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                profileProgressBar.setVisibility(View.GONE);
                //dataRetrieveRequestFail();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public void logout(View view){
        AccountKit.logOut();
        Intent intent=new Intent(getApplicationContext(), LauncherActivity.class);
        startActivity(intent);
        finish();
        Toast.makeText(getApplicationContext(), "Sign Out", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            //TODO:
        }

        return super.onOptionsItemSelected(item);
    }
}
