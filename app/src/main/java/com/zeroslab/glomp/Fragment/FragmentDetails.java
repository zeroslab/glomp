package com.zeroslab.glomp.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zeroslab.glomp.Adapter.AdapterWeekDay;
import com.zeroslab.glomp.Model.WeekDayItem;
import com.zeroslab.glomp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentDetails extends AppCompatActivity {

    ListView listView;
    ArrayList<WeekDayItem> itemLists;
    String key = "AIzaSyAR3eYAvpCDklJGwhLpcdESwg28DllYQSY";
    TextView detailsName, detailsAddress, detailsRating, detailsDistance, detailsPhone, detailsWebsite, detailsStatus;
    ImageView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        detailsName=(TextView)findViewById(R.id.finalName);
        detailsAddress=(TextView)findViewById(R.id.finalVicinity);
        detailsRating=(TextView)findViewById(R.id.finalRating);
        detailsDistance=(TextView)findViewById(R.id.finalDistance);
        detailsPhone=(TextView)findViewById(R.id.finalPhone);
        detailsWebsite=(TextView)findViewById(R.id.finalWebsite);
        detailsStatus=(TextView)findViewById(R.id.finalStatus);
        mapView=(ImageView)findViewById(R.id.finalMapView);
        listView = (ListView) findViewById(R.id.recyclerViewWeek);
        itemLists = new ArrayList<>();

        placeUrlStringBuilder(getIntent().getStringExtra("place_id"), key);
    }

    public void placeUrlStringBuilder(String placeID, String key) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        googlePlacesUrl.append("placeid=" + placeID);
        googlePlacesUrl.append("&key=" + key);

        String url = googlePlacesUrl.toString();
        jsonDetailsRequest(url);
    }

    public void jsonDetailsRequest(String placeIdUrl){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(placeIdUrl, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    JSONObject jsonObject1=jsonObject.getJSONObject("result");

                    JSONObject geometry=jsonObject1.getJSONObject("geometry");
                    JSONObject location=geometry.getJSONObject("location");
                    JSONObject opening_hours=jsonObject1.getJSONObject("opening_hours");

                    detailsName.setText(jsonObject1.getString("name"));
                    detailsAddress.setText(jsonObject1.getString("vicinity"));
                    detailsRating.setText(jsonObject1.getString("rating"));
                    detailsPhone.setText(jsonObject1.getString("international_phone_number"));
                    detailsWebsite.setText(jsonObject1.getString("website"));

                    final String url=jsonObject1.getString("url");

                    SharedPreferences sharedPreferences=getSharedPreferences("location", MODE_PRIVATE);

                    Location locationA=new Location("LocA");

                    try {
                        locationA.setLatitude(Double.parseDouble(sharedPreferences.getString("latitude", "No Data Found")));
                        locationA.setLongitude(Double.parseDouble(sharedPreferences.getString("longitude", "No Data Found")));
                    } catch (Exception e){

                    }

                    Location locationB=new Location("LocB");
                    locationB.setLatitude(Double.parseDouble(location.getString("lat")));
                    locationB.setLongitude(Double.parseDouble(location.getString("lng")));

                    float distance=locationA.distanceTo(locationB)/1000;

                    if (distance<1){
                        detailsDistance.setText(String.valueOf(String.format("%.2f", distance*1000))+"m");
                    } else if (distance>1){
                        detailsDistance.setText(String.valueOf(String.format("%.2f", distance))+"km");
                    }

                    String openStatus=opening_hours.getString("open_now");

                    if (openStatus!=null){
                        if (openStatus.equals("false")){
                            detailsStatus.setText("Now Close");
                        } else if (openStatus.equals("true")){
                            detailsStatus.setText("Now Open");
                        }
                    } else {
                        detailsStatus.setText("-");
                    }

                    mapView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent=new Intent(getApplicationContext(), FragmentMapView.class);
                            intent.putExtra("map_url", url);
                            startActivity(intent);
                        }
                    });

                    weekStatusUrlStringBuilder(jsonObject1.getString("place_id"), key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void weekStatusUrlStringBuilder(String placeID, String key) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        googlePlacesUrl.append("placeid=" + placeID);
        googlePlacesUrl.append("&key=" + key);

        String url = googlePlacesUrl.toString();
        getWeekStatus(url);
    }

    public void getWeekStatus(String JSON_URL){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(JSON_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");
                    JSONObject jsonObject2=jsonObject1.getJSONObject("opening_hours");
                    //JSONArray jsonArray=jsonObject2.getJSONArray("weekday_text");

                    JSONArray jsonArray=(JSONArray)jsonObject2.get("weekday_text");

                    for(int i=0; i<jsonArray.length(); i++){

                        itemLists.add(new WeekDayItem(
                                jsonArray.get(i).toString()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AdapterWeekDay arrayAdapter=new AdapterWeekDay(getApplicationContext(), R.layout.row_week_day, itemLists);
                listView.setAdapter(arrayAdapter);

//                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                    }
//                });
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
