package com.zeroslab.glomp.Fragment;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.zeroslab.glomp.Adapter.RecyclerTouchListener;
import com.zeroslab.glomp.Adapter.RecyclerViewAdapter;
import com.zeroslab.glomp.MainActivity;
import com.zeroslab.glomp.Model.ItemList;
import com.zeroslab.glomp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FragmentNearestList extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    RecyclerView recyclerView;
    ArrayList<ItemList> itemLists;
    ProgressBar listProgressBar;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationRequest mLocationRequest;

    Double latitude;
    Double longitude;
    String PROXIMITY_RADIUS;
    String nearbyPlace;
    final static private String key = "AIzaSyAR3eYAvpCDklJGwhLpcdESwg28DllYQSY";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_nearest_list);

        listProgressBar=(ProgressBar)findViewById(R.id.listProgressBar);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(500);

        recyclerView = (RecyclerView) findViewById(R.id.placeListRecyclerViewItem);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void jsonRequest(String JSON_URL){
        listProgressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(JSON_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    JSONArray jsonArray = jsonObject.getJSONArray("results");

                    itemLists = new ArrayList<>();
                    itemLists.clear();

                    for(int i=0; i<jsonArray.length(); i++){
                        JSONObject itemListObject = jsonArray.getJSONObject(i);
                        JSONObject geometry=itemListObject.getJSONObject("geometry");
                        JSONObject location=geometry.getJSONObject("location");

                        itemLists.add(new ItemList(
                                itemListObject.getString("name"),
                                itemListObject.getString("vicinity"),
                                location.getString("lat"),
                                location.getString("lng"),
                                itemListObject.getString("place_id")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                RecyclerViewAdapter arrayAdapter=new RecyclerViewAdapter(getApplicationContext(), itemLists);
                recyclerView.setAdapter(arrayAdapter);
                listProgressBar.setVisibility(View.GONE);

                recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new MainActivity.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Intent intent=new Intent(getApplicationContext(), FragmentDetails.class);
                        intent.putExtra("place_id", itemLists.get(position).getPlaceIdItemList());
                        startActivity(intent);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listProgressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Connection Problem",Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null) {

            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();

            //getgeolocal(latitude, longitude);

            //Toast.makeText(getApplicationContext(), String.valueOf(latitude) + ", " + String.valueOf(longitude), Toast.LENGTH_SHORT).show();

            sharedPreferences=getSharedPreferences("location", MODE_PRIVATE);
            editor=sharedPreferences.edit();
            editor.putString("latitude", String.valueOf(latitude));
            editor.putString("longitude", String.valueOf(longitude));
            editor.commit();

            nearbyPlace=getIntent().getStringExtra("item_code");
            PROXIMITY_RADIUS=getIntent().getStringExtra("radius");

            stringBuilder(latitude, longitude, nearbyPlace, PROXIMITY_RADIUS, key);

        } else {

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(FragmentNearestList.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    private void getgeolocal(double lat, double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cityName = addresses.get(0).getLocality();

        Toast.makeText(getApplicationContext(), cityName, Toast.LENGTH_SHORT).show();
    }



    public void stringBuilder(Double latitude, Double longitude, String nearbyPlace, String PROXIMITY_RADIUS, String key) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + nearbyPlace); //exmple "hospital"
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + key);

        String url = googlePlacesUrl.toString();
        jsonRequest(url);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Toast.makeText(MainActivity.this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        //Toast.makeText(getApplicationContext(), "GPS Start", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        //Toast.makeText(getApplicationContext(), "GPS Stop", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}