package com.zeroslab.glomp.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.zeroslab.glomp.R;

public class SearchActivity extends AppCompatActivity {

    String[]  array_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        array_name=getResources().getStringArray(R.array.ItemList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.row_spinner, R.id.rowSpinnerText, array_name);
        Spinner spinner=(Spinner)findViewById(R.id.searchSpinner);
        spinner.setAdapter(adapter);
    }
}
