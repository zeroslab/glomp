package com.zeroslab.glomp.Adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeroslab.glomp.Model.ItemList;
import com.zeroslab.glomp.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Farjanul Nayem on 25-Aug-17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    public ArrayList<ItemList> item;
    public Context context;

    public RecyclerViewAdapter(Context context, ArrayList<ItemList> list) {
        this.item = list;
        this.context=context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(layoutView);

        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {

        SharedPreferences sharedPreferences=context.getSharedPreferences("location", MODE_PRIVATE);

        Location locationA=new Location("locA");

        locationA.setLatitude(Double.parseDouble(sharedPreferences.getString("latitude", "No Data Found")));
        locationA.setLongitude(Double.parseDouble(sharedPreferences.getString("longitude", "No Data Found")));

        Location locationB=new Location("locB");
        locationB.setLatitude(Double.parseDouble(item.get(position).getLatitudeItemList()));
        locationB.setLongitude(Double.parseDouble(item.get(position).getLongitudeItemList()));

        float distance=locationA.distanceTo(locationB)/1000;

        //animation
        //YoYo.with(Techniques.ZoomInRight.getAnimator()).playOn(holder.cardView);
        holder.placeName.setText(item.get(position).getNameItemList());
        holder.placeAddress.setText(item.get(position).getVicinityItemList());

        if (distance<1){
            holder.placeDistance.setText(String.valueOf(String.format("%.0f", distance*1000)) +"m");
        } else if (distance>1){
            holder.placeDistance.setText(String.valueOf(String.format("%.2f", distance))+"km");
        }
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView placeName, placeAddress, placeDistance;

        private RecyclerViewHolder(View itemView) {
            super(itemView);

            placeName =itemView.findViewById(R.id.placeName);
            placeAddress=itemView.findViewById(R.id.placeAddress);
            placeDistance=itemView.findViewById(R.id.placeDistance);
        }
    }

    public class ViewHolderForUnderRecyclerViewLayout extends RecyclerView.ViewHolder{


        public ViewHolderForUnderRecyclerViewLayout(View view) {
            super(view);
        }
    }
}
