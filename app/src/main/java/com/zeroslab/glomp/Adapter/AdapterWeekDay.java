package com.zeroslab.glomp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zeroslab.glomp.Model.WeekDayItem;
import com.zeroslab.glomp.R;

import java.util.ArrayList;

/**
 * Created by Farjanul Nayem on 12-Sep-17.
 */

public class AdapterWeekDay extends ArrayAdapter<WeekDayItem> {
    ArrayList<WeekDayItem> itemLists;
    Context context;
    int resource;

    public AdapterWeekDay(Context context, int resource, ArrayList<WeekDayItem> itemLists) {
        super(context, resource, itemLists);
        this.itemLists = itemLists;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_week_day, null, true);
        }

        WeekDayItem itemList = getItem(position);

        TextView textViewId = (TextView) convertView.findViewById(R.id.weekDay);

        textViewId.setText(itemList.getDay());

//        CardView cardView=(CardView)convertView.findViewById(R.id.weekCard);
//        Animation animation= AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
//        cardView.setAnimation(animation);

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }
}
