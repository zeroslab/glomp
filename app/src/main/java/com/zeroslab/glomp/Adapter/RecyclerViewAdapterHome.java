package com.zeroslab.glomp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeroslab.glomp.Model.HomeModel;
import com.zeroslab.glomp.R;

import java.util.List;

/**
 * Created by Farjanul Nayem on 11-Sep-17.
 */

public class RecyclerViewAdapterHome extends RecyclerView.Adapter<RecyclerViewAdapterHome.RecyclerViewHolder> {

    private List<HomeModel> item;
    private Context context;

    public RecyclerViewAdapterHome(Context context, List<HomeModel> item) {
        this.item = item;
        this.context=context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grid, null);

        return new RecyclerViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {

        holder.gridName.setText(item.get(position).getTitle());
        holder.gridName.setSelected(true);
        holder.gridIcon.setImageResource(item.get(position).getIcon());
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView gridName;
        ImageView gridIcon;

        private RecyclerViewHolder(View view) {
            super(view);
            gridName=view.findViewById(R.id.gridName);
            gridIcon=view.findViewById(R.id.gridIcon);
        }
    }
}
