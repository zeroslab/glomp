package com.zeroslab.glomp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;
import com.zeroslab.glomp.Adapter.RecyclerTouchListener;
import com.zeroslab.glomp.Adapter.RecyclerViewAdapterHome;
import com.zeroslab.glomp.Fragment.FragmentNearestList;
import com.zeroslab.glomp.Fragment.FragmentProfile;
import com.zeroslab.glomp.Fragment.SearchActivity;
import com.zeroslab.glomp.Model.HomeModel;
import com.zeroslab.glomp.Model.Weather;
import com.zeroslab.glomp.ProfileUpdate.AgeUpdate;
import com.zeroslab.glomp.ProfileUpdate.EmailUpdate;
import com.zeroslab.glomp.ProfileUpdate.GenderUpdate;
import com.zeroslab.glomp.ProfileUpdate.LivingLocationUpdate;
import com.zeroslab.glomp.ProfileUpdate.NameUpdate;
import com.zeroslab.glomp.ProfileUpdate.ProfessionUpdate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, NavigationView.OnNavigationItemSelectedListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationRequest mLocationRequest;

    Double latitude;
    Double longitude;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ArrayList<Weather> weather;
    TextView textStatus, tvtemp, today_high, today_low, location;
    ImageView statusicon, circlerImage;

    String lastBuildDate;
    String city;
    String country;
    String temp;
    String text;
    String geoCityName;
    Context context;

    RecyclerView recyclerView;
    List<HomeModel> homeModels;
    String[] itemNameList;
    int[] itemIconList = {
            R.drawable.ic_atm,
            R.drawable.ic_travel,
            R.drawable.ic_aquarium,
            R.drawable.ic_artgallary,
            R.drawable.ic_bakery,
            R.drawable.ic_dollar,
            R.drawable.ic_liquor,
            R.drawable.ic_open_book,
            R.drawable.ic_beauty_salon,
            R.drawable.ic_bus,
            R.drawable.ic_cardealer,
            R.drawable.ic_carrental,
            R.drawable.ic_car_repair,
            R.drawable.ic_cemetery,
            R.drawable.ic_church,
            R.drawable.ic_open_book,
            R.drawable.ic_court,
            R.drawable.ic_shoppingmall,
            R.drawable.ic_electronicstore,
            R.drawable.ic_embassy,
            R.drawable.ic_firestation,
            R.drawable.ic_flowerstore,
            R.drawable.ic_furniture,
            R.drawable.ic_gas_station,
            R.drawable.ic_govoffice,
            R.drawable.ic_gym,
            R.drawable.ic_salon,
            R.drawable.ic_open_book,
            R.drawable.ic_hospital,
            R.drawable.ic_jewelry_store,
            R.drawable.ic_iron,
            R.drawable.ic_lawyer,
            R.drawable.ic_library,
            R.drawable.ic_liquor,
            R.drawable.ic_mosque,
            R.drawable.ic_theater,
            R.drawable.ic_museum,
            R.drawable.ic_newsservice,
            R.drawable.ic_park,
            R.drawable.ic_pharmacy,
            R.drawable.ic_physiotherapist,
            R.drawable.ic_police,
            R.drawable.ic_postbox,
            R.drawable.ic_radio,
            R.drawable.ic_realestate,
            R.drawable.ic_restaurant,
            R.drawable.ic_open_book,
            R.drawable.ic_shoppingmall,
            R.drawable.ic_stadium,
            R.drawable.ic_taxi,
            R.drawable.ic_temple,
            R.drawable.ic_television,
            R.drawable.ic_train,
            R.drawable.ic_travel_agency,
            R.drawable.ic_university,
            R.drawable.ic_zoo};

    LinearLayout weatherLayout;
    String lat, lan;
    ImageView item1, item2, item3, item4, item5, item6, item7, item8, item9;

    String phoneNo, name, email, gender, age, profession, livingLocation, toolbarName;
    String baseUrl = "http://solidhit.org/apps/niketon/api.php?phone=";
    boolean isPause = false;
    int i = 3;
    Handler handler;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountKit.initialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(500);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        handler = new Handler();
        weatherLayout = (LinearLayout) findViewById(R.id.myWeatherLayout);
        textStatus = (TextView) findViewById(R.id.textStatus);
        statusicon = (ImageView) findViewById(R.id.statusicon);
        today_high = (TextView) findViewById(R.id.today_high);
        today_low = (TextView) findViewById(R.id.today_low);
        tvtemp = (TextView) findViewById(R.id.temp);
        location = (TextView) findViewById(R.id.location);
        circlerImage = (ImageView) findViewById(R.id.circlerImage);
        Picasso.with(getApplicationContext()).load(R.drawable.remote_circle).resize(300, 300).into(circlerImage);

        item1 = (ImageView) findViewById(R.id.txt1);
        item2 = (ImageView) findViewById(R.id.txt2);
        item3 = (ImageView) findViewById(R.id.txt3);
        item4 = (ImageView) findViewById(R.id.txt4);
        item5 = (ImageView) findViewById(R.id.txt5);
        item6 = (ImageView) findViewById(R.id.txt6);
        item7 = (ImageView) findViewById(R.id.txt7);
        item8 = (ImageView) findViewById(R.id.txt8);
        item9 = (ImageView) findViewById(R.id.txt9);

//        Picasso.with(getApplicationContext()).load(R.drawable.ic_shoppingmall).resize(40, 40).into(item1);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_firestation).resize(40, 40).into(item2);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_pharmacy).resize(40, 40).into(item3);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_hospital).resize(40, 40).into(item4);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_temple).resize(40, 40).into(item5);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_gas_station).resize(40, 40).into(item6);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_police).resize(40, 40).into(item7);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_mosque).resize(40, 40).into(item8);
//        Picasso.with(getApplicationContext()).load(R.drawable.ic_restaurant).resize(70, 70).into(item9);

        setCircleButton();
        runApp();
        getLatLan();
        LocationHardwarePermission();
    }

    private void runApp() {
        Resources resources = getResources();
        itemNameList = resources.getStringArray(R.array.ItemList);

        recyclerView = (RecyclerView) findViewById(R.id.homeRecyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.invalidate();

        homeModels = new ArrayList<>();
        for (int i = 0; i < itemNameList.length; i++) {
            homeModels.add(new HomeModel(
                    itemNameList[i],
                    itemIconList[i]
            ));
        }

        RecyclerViewAdapterHome adapter = new RecyclerViewAdapterHome(getApplicationContext(), homeModels);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                getItemList(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        //getLatLan();
        //getCallBack();

        //connection();
    }

    public void getCallBack() {
        try {
            AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                @Override
                public void onSuccess(final Account account) {
                    String id = account.getId();

                    final PhoneNumber number = account.getPhoneNumber();
                    String phoneNumber = number == null ? null : number.toString();

                    String url = baseUrl + phoneNumber;
                    getProfileData(url);
                }

                @Override
                public void onError(final AccountKitError error) {
                    //TODO: error handling
                }
            });
        } catch (Exception e) {

        }
    }

    private void getProfileData(final String url) {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new com.android.volley.Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jsonObject = response.getJSONObject(i);

                        name = jsonObject.getString("name");
                        phoneNo = jsonObject.getString("phone");
                        email = jsonObject.getString("email");
                        gender = jsonObject.getString("gender");
                        age = jsonObject.getString("age");
                        profession = jsonObject.getString("profession");
                        livingLocation = jsonObject.getString("living");
                    }

                    if (name == null || name.equals("null")) {
                        Handler handler = new Handler();
                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), NameUpdate.class));
                            }
                        };

                        if (!isPause) {
                            handler.postDelayed(updateProfileName, 10000);
                        } else {
                            handler.removeCallbacks(updateProfileName);
                        }

                    } else if (email == null || email.equals("null")) {

                        Handler handler = new Handler();
                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), EmailUpdate.class));

                            }
                        };

                        if (!isPause) {
                            handler.postDelayed(updateProfileName, 10000);
                        } else {
                            handler.removeCallbacks(updateProfileName);
                        }

                    } else if (age == null || age.equals("null")) {
                        Handler handler = new Handler();
                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), AgeUpdate.class));
                            }
                        };

                        if (!isPause) {
                            handler.postDelayed(updateProfileName, 10000);
                        } else {
                            handler.removeCallbacks(updateProfileName);
                        }
                    } else if (gender == null || gender.equals("null")) {

                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), GenderUpdate.class));
                            }
                        };

                        if (i == 4) {
                            handler.postDelayed(updateProfileName, 2000);
                        } else if (i == 5) {
                            handler.removeCallbacks(updateProfileName);
                        }

                    } else if (profession == null || profession.equals("null")) {
                        Handler handler = new Handler();
                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), ProfessionUpdate.class));
                            }
                        };

                        if (!isPause) {
                            handler.postDelayed(updateProfileName, 10000);
                        } else {
                            handler.removeCallbacks(updateProfileName);
                        }
                    } else if (livingLocation == null || livingLocation.equals("null")) {
                        Handler handler = new Handler();
                        Runnable updateProfileName = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), LivingLocationUpdate.class));
                            }
                        };

                        if (!isPause) {
                            handler.postDelayed(updateProfileName, 10000);
                        } else {
                            handler.removeCallbacks(updateProfileName);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO: Handle Something
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void getLatLan() {
        sharedPreferences = getSharedPreferences("location", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.commit();

        try {
            lat = sharedPreferences.getString("latitude", "Not Data Found");
            lan = sharedPreferences.getString("longitude", "Not Data Found");
        } catch (Exception e) {

        }

        if (!lat.equals("Not Data Found") || !lan.equals("Not Data Found")) {
            getgeolocal(Double.parseDouble(lat), Double.parseDouble(lan));
        } else {
            String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + "dhaka" + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            getWeatherData(url);
        }
    }

    private void getgeolocal(double lat, double lan) {

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lat, lan, 1);
            geoCityName = addresses.get(0).getLocality();
            String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + geoCityName + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            getWeatherData(url);
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Restart Device For Weather", Toast.LENGTH_SHORT).show();
        }
    }

    private void getWeatherData(String url) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    JSONObject queryObject = jsonObject.getJSONObject("query");
                    JSONObject resultsObject = queryObject.getJSONObject("results");
                    JSONObject channelObject = resultsObject.getJSONObject("channel");
                    JSONObject locationObject = channelObject.getJSONObject("location");
                    JSONObject itemObject = channelObject.getJSONObject("item");
                    JSONObject conditionObject = itemObject.getJSONObject("condition");

                    lastBuildDate = channelObject.getString("lastBuildDate");
                    city = locationObject.getString("city");
                    country = locationObject.getString("country");
                    temp = conditionObject.getString("temp");
                    text = conditionObject.getString("text");

                    JSONArray forecastArray = itemObject.getJSONArray("forecast");

                    for (int i = 0; i < forecastArray.length(); i++) {
                        JSONObject forecastObject = forecastArray.getJSONObject(i);

                        weather = new ArrayList<>();
                        weather.add(new Weather(
                                forecastObject.getString("high"),
                                forecastObject.getString("low"),
                                text,
                                temp,
                                city,
                                country
                        ));
                    }

                    weatherLayout.setVisibility(View.VISIBLE);

                    DecimalFormat df = new DecimalFormat("#");
                    double tempInt = Double.parseDouble(temp);
                    double tempLow = Double.parseDouble(weather.get(0).getLow());
                    double tempHigh = Double.parseDouble(weather.get(0).getHigh());

                    String status = weather.get(0).getStatus();

                    textStatus.setText(text);
                    today_high.setText("High: " + df.format((5.0 / 9.0) * (tempHigh - 32.0)) + (char) 0x00B0 + "C");
                    today_low.setText("Low: " + df.format((5.0 / 9.0) * (tempLow - 32.0)) + (char) 0x00B0 + "C");
                    tvtemp.setText(df.format((5.0 / 9.0) * (tempInt - 32.0)) + (char) 0x00B0 + "C");
                    location.setText(city + ", " + country);

                    if (status.equals("Mostly Cloudy")) {
                        statusicon.setImageResource(R.drawable.ic_cloudy);
                    } else if (status.equals("Rain") || status.equals("Partly Rain") || status.equals("Mostly Rain") || status.equals("Showers")) {
                        statusicon.setImageResource(R.drawable.ic_rain);
                    } else if (status.equals("Thunderstorms") || status.equals("Scattered Thunderstorms")) {
                        statusicon.setImageResource(R.drawable.ic_thunderstorm);
                    } else if (status.equals("Breezy") || status.equals("Mostly Clear") || status.equals("Partly Clear") || status.equals("Clear") || status.equals("Sunny") || status.equals("Most Sunny") || status.equals("Partly Sunny")) {
                        statusicon.setImageResource(R.drawable.ic_happy_sun);
                    } else if (status.equals("Cloudy") || status.equals("Partly Cloudy")) {
                        statusicon.setImageResource(R.drawable.ic_cloudy);
                    } else {
                        statusicon.setImageResource(R.drawable.ic_rounded_block_sign);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    weatherLayout.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                weatherLayout.setVisibility(View.GONE);
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonObjectRequest);
    }


    public void getItemList(int position) {
        if (position == 0) {
            String item = "atm", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 1) {
            String item = "airport", radius = "20000";
            gotoNearByList(item, radius);
        } else if (position == 2) {
            String item = "aquarium", radius = "10000";
            gotoNearByList(item, radius);
        } else if (position == 3) {
            String item = "art_gallery", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 4) {
            String item = "bakery", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 5) {
            String item = "bank", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 6) {
            String item = "bar", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 7) {
            String item = "book_store", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 8) {
            String item = "beauty_salon", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 9) {
            String item = "bus_station", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 10) {
            String item = "car_dealer", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 11) {
            String item = "car_rental", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 12) {
            String item = "car_repair", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 13) {
            String item = "cemetery", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 14) {
            String item = "church", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 15) {
            String item = "college", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 16) {
            String item = "courthouse", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 17) {
            String item = "departmental_store", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 18) {
            String item = "electronics_store", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 19) {
            String item = "embassy", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 20) {
            String item = "fire_station", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 21) {
            String item = "flower_store", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 22) {
            String item = "furniture_store", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 23) {
            String item = "gas_station", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 24) {
            String item = "local_office", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 25) {
            String item = "gym", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 26) {
            String item = "hair_salon", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 27) {
            String item = "high_school", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 28) {
            String item = "hospital", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 29) {
            String item = "jewelry_store", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 30) {
            String item = "laundry", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 31) {
            String item = "lawyer", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 32) {
            String item = "library", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 33) {
            String item = "liquor_store", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 34) {
            String item = "mosque", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 35) {
            String item = "movie_theater", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 36) {
            String item = "museum", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 37) {
            String item = "news_service", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 38) {
            String item = "park", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 39) {
            String item = "pharmacy", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 40) {
            String item = "physiotherapist", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 41) {
            String item = "police", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 42) {
            String item = "post_office", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 43) {
            String item = "radio", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 44) {
            String item = "real_state", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 45) {
            String item = "restaurant", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 46) {
            String item = "school", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 47) {
            String item = "shopping_mall", radius = "1000";
            gotoNearByList(item, radius);
        } else if (position == 48) {
            String item = "stadium", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 49) {
            String item = "taxi_stand", radius = "3000";
            gotoNearByList(item, radius);
        } else if (position == 50) {
            String item = "temple", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 51) {
            String item = "television_station", radius = "500";
            gotoNearByList(item, radius);
        } else if (position == 52) {
            String item = "train_station", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 53) {
            String item = "travel_agency", radius = "2000";
            gotoNearByList(item, radius);
        } else if (position == 54) {
            String item = "university", radius = "5000";
            gotoNearByList(item, radius);
        } else if (position == 55) {
            String item = "zoo", radius = "5000";
            gotoNearByList(item, radius);
        }
    }

    private void setCircleButton() {
        item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "shopping_mall", radius = "1000";
                gotoNearByList(item, radius);
            }
        });

        item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "fire_station", radius = "2000";
                gotoNearByList(item, radius);
            }
        });

        item3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "pharmacy", radius = "1000";
                gotoNearByList(item, radius);
            }
        });

        item4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "hospital", radius = "1000";
                gotoNearByList(item, radius);
            }
        });

        item5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "temple", radius = "500";
                gotoNearByList(item, radius);
            }
        });

        item6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "police", radius = "2000";
                gotoNearByList(item, radius);
            }
        });

        item7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "gas_station", radius = "3000";
                gotoNearByList(item, radius);
            }
        });

        item8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "mosque", radius = "1000";
                gotoNearByList(item, radius);
            }
        });

        item9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = "restaurant", radius = "1000";
                gotoNearByList(item, radius);
            }
        });
    }

    private void gotoNearByList(String item, String radius) {
        Intent intent = new Intent(getApplicationContext(), FragmentNearestList.class);
        intent.putExtra("item_code", item);
        intent.putExtra("radius", radius);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Search " + item + " within " + radius + "m", Toast.LENGTH_LONG).show();
    }

    private void LocationHardwarePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 123);

        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 123: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getApplicationContext(), "Permission Required", Toast.LENGTH_SHORT).show();
                    finish();
                }

                break;
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null) {

            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();

            sharedPreferences = getSharedPreferences("location", MODE_PRIVATE);
            editor = sharedPreferences.edit();
            editor.putString("latitude", String.valueOf(latitude));
            editor.putString("longitude", String.valueOf(longitude));

            editor.commit();

        } else {

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        MainActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(MainActivity.this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
        isPause=true;
        i=5;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        //Toast.makeText(getApplicationContext(), "GPS Start", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        //Toast.makeText(getApplicationContext(), "GPS Stop", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_explore:
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                    return true;
                case R.id.navigation_profile:
                    startActivity(new Intent(getApplicationContext(), FragmentProfile.class));
                    return true;
            }
            return false;
        }

    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            //TODO:
            AccountKit.logOut();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public interface ClickListener{
        void onClick(View view,int position);
        void onLongClick(View view,int position);
    }
}
