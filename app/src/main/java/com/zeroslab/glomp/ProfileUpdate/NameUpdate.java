package com.zeroslab.glomp.ProfileUpdate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.zeroslab.glomp.Fragment.FragmentProfile;
import com.zeroslab.glomp.MainActivity;
import com.zeroslab.glomp.R;

import java.util.HashMap;
import java.util.Map;

public class NameUpdate extends AppCompatActivity {

    Button skip, finish;
    EditText infoET;
    TextView warning, updateProfileIntro;
    String etData;
    String baseUrl="http://solidhit.org/apps/niketon/updateName.php?phone=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        AccountKit.initialize(getApplicationContext());
        setContentView(R.layout.activity_profile_name);

        skip=(Button)findViewById(R.id.skip_update_name_button);
        finish=(Button)findViewById(R.id.finish_update_name_button);
        infoET =(EditText)findViewById(R.id.nameUpdate);
        infoET.setHint("Type Your Name");
        infoET.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        updateProfileIntro=(TextView)findViewById(R.id.updateProfileIntro);
        updateProfileIntro.setText("Hi, I am Glomp.\nYou?");
        warning=(TextView)findViewById(R.id.warning);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etData = infoET.getText().toString();
                if (etData.length()>=3){
                    getCallBack(etData);
                } else {
                    warning.setText(getString(R.string.name_type_warning));
                }
            }
        });
    }

    public void getCallBack(final String etData){
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String id = account.getId();

                final PhoneNumber number = account.getPhoneNumber();
                String phoneNumber = number == null ? null : number.toString();
                setRegUser(baseUrl+phoneNumber, etData);
            }

            @Override
            public void onError(final AccountKitError error) {
                //TODO: error handling
            }
        });
    }

    private void setRegUser(String url, final String etData){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), FragmentProfile.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG ).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();

                map.put("name", etData);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}
